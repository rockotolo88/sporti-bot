from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from core_config import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('app_bot.urls')),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

