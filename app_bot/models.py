from datetime import date

from django.db import models
from django.db.models import CASCADE


class SlackUser(models.Model):
    """Model representing a Slack user."""
    GENDER_CHOICES = (
        (1, "male"),
        (2, "female"),
    )

    user_id = models.CharField(max_length=100, unique=True, help_text="User ID from Slack chat")
    name = models.CharField(max_length=50)
    birth_date = models.DateField()
    gender = models.IntegerField(choices=GENDER_CHOICES)
    height = models.FloatField()

    def __str__(self):
        return f"{self.name} - {self.user_id}"

    @property
    def age(self):
        """Calculate age based on birth date."""
        today = date.today()
        age = today.year - self.birth_date.year - (
                (today.month, today.day) < (self.birth_date.month, self.birth_date.day))
        return age


class Exercise(models.Model):
    """Model representing an exercise."""
    BodyPart = (
        (1, "legs"),
        (2, "chest"),
        (3, "back"),
        (4, "biceps"),
        (5, "triceps"),
        (6, "shoulders"),
    )
    name = models.CharField(max_length=50)
    image = models.ImageField(upload_to='exercises/')
    is_large_muscle = models.BooleanField()
    body_part = models.IntegerField(choices=BodyPart)

    def __str__(self):
        return self.name


class Training(models.Model):
    """Model representing a training session."""
    user = models.ForeignKey(SlackUser, on_delete=CASCADE)
    exercise = models.ForeignKey(Exercise, on_delete=CASCADE)
    num_sets = models.PositiveIntegerField()
    num_repetitions = models.PositiveIntegerField()
    weight = models.FloatField()

    def __str__(self):
        return f"{self.exercise.name}, {self.user}"


class ProgressTraining(models.Model):
    """Model representing a user's progress over time."""
    training = models.ForeignKey(Training, on_delete=CASCADE)
    date = models.DateField(auto_now_add=True)
    num_sets = models.PositiveIntegerField()
    num_repetitions = models.PositiveIntegerField()
    weight = models.FloatField()

    def __str__(self):
        return f"{self.date} - {self.training.exercise.name}, {self.training.user}, {self.weight}kg"
