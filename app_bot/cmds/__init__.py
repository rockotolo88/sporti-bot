from .info import InfoCommand
from .exercise import ExerciseListCommand
from .training import TrainingStartCommand
from .slack_user import RegistrationUserCommand
