from typing import Dict

from django.core.exceptions import ValidationError
from slack_sdk.web import SlackResponse

from app_bot.cmds.mixins import CommandMixin
from app_bot.models import SlackUser


class RegistrationUserCommand(CommandMixin):
    command = ['-r', '-registration']
    command_description = """
    Použijte tuto příkaz k registraci jako uživatele tohoto bota. 
    """

    def handle(self, client, message) -> SlackResponse:
        user_id = message['user']

        if self.check_registered(user_id):
            return self.send_user_already_registered(self.channel_id, client, user_id)

        return client.chat_postMessage(
            channel=message['channel'],
            text="Click the button below to register",
            blocks=[
                {
                    "type": "actions",
                    "elements": [
                        {
                            "type": "button",
                            "text": {
                                "type": "plain_text",
                                "text": "Register"
                            },
                            "action_id": "open_registration_form"
                        }
                    ]
                }
            ]
        )

    @staticmethod
    def get_slack_user_form() -> Dict:
        return {
            "type": "modal",
            "callback_id": "registration_form",
            "title": {
                "type": "plain_text",
                "text": "Registration"
            },
            "submit": {
                "type": "plain_text",
                "text": "Submit"
            },
            "blocks": [
                {
                    "type": "input",
                    "block_id": "name_block",
                    "element": {
                        "type": "plain_text_input",
                        "action_id": "name_action",
                        "placeholder": {
                            "type": "plain_text",
                            "text": "Enter your name"
                        }
                    },
                    "label": {
                        "type": "plain_text",
                        "text": "Name"
                    }
                },
                {
                    "type": "input",
                    "block_id": "birthdate_block",
                    "element": {
                        "type": "datepicker",
                        "action_id": "birthdate_action",
                        "placeholder": {
                            "type": "plain_text",
                            "text": "Enter your birth date"
                        }
                    },
                    "label": {
                        "type": "plain_text",
                        "text": "Birth Date"
                    }
                },
                {
                    "type": "input",
                    "block_id": "gender_block",
                    "element": {
                        "type": "radio_buttons",
                        "action_id": "gender_action",
                        "options": [
                            {
                                "text": {
                                    "type": "plain_text",
                                    "text": "Man"
                                },
                                "value": "1"
                            },
                            {
                                "text": {
                                    "type": "plain_text",
                                    "text": "Woman"
                                },
                                "value": "2"
                            }
                        ]
                    },
                    "label": {
                        "type": "plain_text",
                        "text": "Gender"
                    }
                },
                {
                    "type": "input",
                    "block_id": "height_block",
                    "element": {
                        "type": "plain_text_input",
                        "action_id": "height_action",
                        "placeholder": {
                            "type": "plain_text",
                            "text": "Enter your height"
                        }
                    },
                    "label": {
                        "type": "plain_text",
                        "text": "Height"
                    }
                }
            ]
        }

    @staticmethod
    def check_registered(user_id: str) -> bool:
        return SlackUser.objects.filter(user_id=user_id).exists()

    @staticmethod
    def send_user_already_registered(channel_id, client, user_id: str) -> SlackResponse:
        return client.chat_postMessage(channel=channel_id, text=f"<@{user_id}> jsi již registrován!")

    @staticmethod
    def create_slack_user(payload) -> bool:
        values = payload['view']['state']['values']
        try:
            SlackUser.objects.create(
                user_id=payload['user']['id'],
                name=values['name_block']['name_action']['value'],
                birth_date=values['birthdate_block']['birthdate_action']['selected_date'],
                gender=int(values['gender_block']['gender_action']['selected_option']['value']),
                height=int(values['height_block']['height_action']['value']),
            )
            return True
        except ValidationError as e:
            print(f"Validation Error: {e}")
            return False
