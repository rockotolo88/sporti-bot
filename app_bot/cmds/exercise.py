import json
from typing import List
from urllib.parse import urljoin

from slack_sdk.web import SlackResponse

from app_bot.cmds.mixins import CommandMixin
from app_bot.models import Exercise, Training, SlackUser
from core_config.settings import DOMAIN


class ExerciseListCommand(CommandMixin):
    command = ['-e', '-exercise']
    command_description = """
    Použijte tento příkaz k zobrazení seznamu všech cvičení dostupných v databázi.
    """

    def handle(self, client, message) -> SlackResponse:
        message = "List of all exercises"
        return client.chat_postMessage(channel=self.channel_id, text=message, blocks=self.blocks)

    @property
    def blocks(self) -> List:
        exercises = Exercise.objects.all().order_by('-body_part')

        blocks = []
        for exercise in exercises:
            image_url = urljoin(DOMAIN, exercise.image.url)

            trainings = Training.objects.filter(exercise=exercise)
            exercise_text = f"*{exercise.name}*\n"
            for t in trainings:
                exercise_text += f"_{t.user.name}:_ " \
                                 f"s[{t.num_sets}], " \
                                 f"r[{t.num_repetitions}], " \
                                 f"w[{int(t.weight) if t.weight.is_integer() else t.weight}]\n"
            exercise_block = [
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": exercise_text
                    },
                    "accessory": {
                        "type": "image",
                        "image_url": image_url,
                        "alt_text": exercise.name
                    }
                },
                {
                    "type": "actions",
                    "elements": [
                        {
                            "type": "button",
                            "text": {
                                "type": "plain_text",
                                "text": f"Edit: {exercise.name}"
                            },
                            "action_id": "open_exercise_form",
                            "value": str(exercise.id)
                        }
                    ]
                }

            ]

            blocks.extend(exercise_block)
        return blocks

    @staticmethod
    def get_exercise_form(exercise_id: int, user_id: str, message_ts: str, callback_id: str, **kwargs) -> dict:
        training = Training.objects.filter(user=SlackUser.objects.get(user_id=user_id), exercise__id=exercise_id)
        exercise = Exercise.objects.get(id=exercise_id)

        weight = ""
        if training.exists():
            t = training.get()
            weight = str(int(t.weight)) if t.weight.is_integer() else str(t.weight)

        data = {
            "exercise_id": exercise_id,
            "message_ts": message_ts,
            "body_parts": [kwargs.get('metadata')]
        }

        return {
            "type": "modal",
            "callback_id": callback_id,
            "title": {
                "type": "plain_text",
                "text": f"{exercise.name[:21]}..." if len(exercise.name) > 25 else f"{exercise.name}"
            },
            "submit": {
                "type": "plain_text",
                "text": "Submit"
            },
            "private_metadata": json.dumps(data),
            "blocks": [
                {
                    "type": "input",
                    "block_id": "num_sets_block",
                    "element": {
                        "type": "plain_text_input",
                        "action_id": "num_sets_action",
                        "initial_value": str(training.get().num_sets) if training.exists() else "",
                        "placeholder": {
                            "type": "plain_text",
                            "text": "Enter nums of sets",
                        }
                    },
                    "label": {
                        "type": "plain_text",
                        "text": "Nums of sets"
                    }
                },
                {
                    "type": "input",
                    "block_id": "num_repetitions_block",
                    "element": {
                        "type": "plain_text_input",
                        "action_id": "num_repetitions_action",
                        "initial_value": str(training.get().num_repetitions) if training.exists() else "",
                        "placeholder": {
                            "type": "plain_text",
                            "text": "Enter nums of repetitions",
                        }
                    },
                    "label": {
                        "type": "plain_text",
                        "text": "Nums of repetitions"
                    }
                },
                {
                    "type": "input",
                    "block_id": "weight_block",
                    "element": {
                        "type": "plain_text_input",
                        "action_id": "weight_action",
                        "initial_value": weight,
                        "placeholder": {
                            "type": "plain_text",
                            "text": "Enter the weight",
                        }
                    },
                    "label": {
                        "type": "plain_text",
                        "text": "Weight"
                    }
                },
            ]
        }

    @staticmethod
    def update_or_create_training(payload: dict) -> bool:
        values = payload['view']['state']['values']
        exercise_id = json.loads(payload['view']['private_metadata'])['exercise_id']
        obj, created = Training.objects.update_or_create(
            user=SlackUser.objects.get(user_id=payload['user']['id']),
            exercise=Exercise.objects.get(id=exercise_id),
            defaults={
                'num_sets': values['num_sets_block']['num_sets_action']['value'],
                'num_repetitions': values['num_repetitions_block']['num_repetitions_action']['value'],
                'weight': values['weight_block']['weight_action']['value'],
            }
        )
        return created
