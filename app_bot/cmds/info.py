from slack_sdk.web import SlackResponse

from app_bot.cmds.exercise import ExerciseListCommand
from app_bot.cmds.mixins import CommandMixin
from app_bot.cmds.slack_user import RegistrationUserCommand
from app_bot.cmds.training import TrainingStartCommand


class InfoCommand(CommandMixin):
    command = ['-i', '-info',
               '-h', '-help']
    command_description = f"commands: *{', '.join(RegistrationUserCommand().command)}* {RegistrationUserCommand().command_description}\n" \
                          f"commands: *{', '.join(ExerciseListCommand().command)}* {ExerciseListCommand().command_description}\n" \
                          f"commands: *{', '.join(TrainingStartCommand().command)}* {TrainingStartCommand().command_description}"

    def handle(self, client, message) -> SlackResponse:
        return client.chat_postMessage(channel=self.channel_id, text=self.command_description)
