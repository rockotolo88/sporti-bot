import json
from typing import Dict, List
from urllib.parse import urljoin

from slack_sdk.web import SlackResponse

from app_bot.cmds.mixins import CommandMixin
from app_bot.models import Training, Exercise, ProgressTraining
from core_config.settings import DOMAIN
from datetime import datetime


class TrainingStartCommand(CommandMixin):
    command = ['-t', '-training']
    command_description = """
    Použitím tohoto příkazu spustíte personalizovaný trénink, založený na vašem výběru velké a malé svalové skupiny. Tento příkaz použijte pouze tehdy, když se chystáte začít trénovat, protože data jsou zaznamenána pro statistiky vašeho tréninku. 
    """

    def handle(self, client, message) -> SlackResponse:
        exercise_block = [
            {
                "type": "actions",
                "elements": [
                    {
                        "type": "button",
                        "text": {
                            "type": "plain_text",
                            "text": "Start training"
                        },
                        "action_id": "open_training_start_form",
                        "value": "Start training"
                    }
                ]
            }

        ]
        return client.chat_postMessage(channel=self.channel_id, blocks=exercise_block)

    @staticmethod
    def get_training_start_form() -> Dict:
        large_muscle = ['Legs', 'Chest', 'Back']
        small_muscle = ['Biceps', 'Triceps', 'Shoulders']
        return {
            "type": "modal",
            "callback_id": "training_start_form",
            "title": {
                "type": "plain_text",
                "text": "Training"
            },
            "submit": {
                "type": "plain_text",
                "text": "Submit"
            },
            "blocks": [
                {
                    "type": "input",
                    "block_id": "large_muscle_block",
                    "element": {
                        "type": "static_select",
                        "action_id": "large_muscle",
                        "initial_option": {
                            "text": {
                                "type": "plain_text",
                                "text": large_muscle[0]
                            },
                            "value": large_muscle[0]
                        },
                        "placeholder": {
                            "type": "plain_text",
                            "text": "Select a muscle of the training",
                        },
                        "options": [
                            {
                                "text": {
                                    "type": "plain_text",
                                    "text": muscle
                                },
                                "value": muscle
                            } for muscle in large_muscle
                        ]
                    },
                    "label": {
                        "type": "plain_text",
                        "text": "Choice the large muscle"
                    }
                },
                {
                    "type": "input",
                    "block_id": "small_muscle_block",
                    "element": {
                        "type": "static_select",
                        "action_id": "small_muscle",
                        "initial_option": {
                            "text": {"type": "plain_text",
                                     "text": small_muscle[0]
                                     },
                            "value": small_muscle[0]
                        },
                        "placeholder": {
                            "type": "plain_text",
                            "text": "Select a muscle of the training",
                        },
                        "options": [
                            {
                                "text": {
                                    "type": "plain_text",
                                    "text": muscle
                                },
                                "value": muscle
                            } for muscle in small_muscle
                        ]
                    },
                    "label": {
                        "type": "plain_text",
                        "text": "Choice the small muscle"
                    }
                },
            ]
        }

    @staticmethod
    def update_or_create_progress_training(large_muscle: str, small_muscle: str, user_id: str, date=None) -> List:
        muscles_ids = [id for id, name in Exercise.BodyPart if name in (large_muscle.lower(), small_muscle.lower())]
        trainings = Training.objects.filter(user__user_id=user_id, exercise__body_part__in=muscles_ids).order_by(
            '-exercise__is_large_muscle')
        if not trainings:
            return []

        data = {"large_muscle": large_muscle, "small_muscle": small_muscle}
        blocks = [
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": f"<@{user_id}>: tvoje cvičení:"
                },
            },
        ]
        datetime_today = datetime.today() if not date else date
        for t in trainings:
            obj, created = ProgressTraining.objects.update_or_create(
                training=t,
                date=datetime_today,
                defaults={
                    'num_sets': t.num_sets,
                    'num_repetitions': t.num_repetitions,
                    'weight': t.weight
                }
            )
            data['date'] = str(obj.date)
            message = f"*{t.exercise.name}*:\n s[{t.num_sets}]," \
                      f" r[{t.num_repetitions}]," \
                      f" w[{int(t.weight) if t.weight.is_integer() else t.weight}]"

            image_url = urljoin(DOMAIN, t.exercise.image.url)
            exercise_block = [
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": message
                    },
                    "accessory": {
                        "type": "image",
                        "image_url": image_url,
                        "alt_text": t.exercise.name
                    },
                },
                {
                    "type": "actions",
                    "elements": [
                        {
                            "type": "button",
                            "text": {
                                "type": "plain_text",
                                "text": f"Edit: {t.exercise.name}"
                            },
                            "action_id": "open_exercise_training_form",
                            "value": json.dumps({"exercise_id": str(t.exercise.id), "metadata": data}),
                        }
                    ],
                }
            ]

            blocks.extend(exercise_block)

        return blocks
