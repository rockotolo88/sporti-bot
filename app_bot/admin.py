from django.contrib import admin
from django.contrib.admin import SimpleListFilter
from django.utils.html import format_html

from .models import SlackUser, Exercise, Training, ProgressTraining


class DateFilter(SimpleListFilter):
    title = 'date'
    parameter_name = 'date'

    def lookups(self, request, model_admin):
        dates = list(set(model_admin.model.objects.values_list('date', flat=True)))
        dates.sort(reverse=True)
        return [(d, d) for d in dates]

    def queryset(self, request, queryset):
        if self.value() is not None:
            return queryset.filter(date__exact=self.value())


@admin.register(SlackUser)
class SlackUserAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'user_id',
        'name',
        'birth_date',
        'gender',
        'height',
    )
    list_filter = ('birth_date',)
    search_fields = ('name',)


@admin.register(Exercise)
class ExerciseAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'get_image', 'is_large_muscle', 'body_part')
    list_filter = ('is_large_muscle',)
    search_fields = ('name',)

    def get_image(self, obj):
        return format_html('<img src="{}" width="50" height="50" />', obj.image.url)

    get_image.short_description = 'Image'


@admin.register(Training)
class TrainingAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'user',
        'exercise',
        'num_sets',
        'num_repetitions',
        'weight',
    )
    list_filter = ('user', 'exercise')


@admin.register(ProgressTraining)
class ProgressTrainingAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'get_image',
        'get_body_part',
        'get_exercise_name',
        'get_user',
        'date',
        'num_sets',
        'num_repetitions',
        'weight',
    )
    list_filter = (DateFilter,)
    list_display_links = ('id', 'get_image', 'get_body_part')
    ordering = ('date',)

    def get_body_part(self, obj):
        return obj.training.exercise.get_body_part_display()

    get_body_part.short_description = 'Body Part'

    def get_user(self, obj):
        return obj.training.user.name

    get_user.short_description = 'User'

    def get_exercise_name(self, obj):
        return obj.training.exercise.name

    get_exercise_name.short_description = 'Exercise'

    def get_image(self, obj):
        return format_html('<img src="{}" width="50" height="50" />', obj.training.exercise.image.url)

    get_image.short_description = 'Image'
