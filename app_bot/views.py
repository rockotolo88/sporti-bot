import json
import urllib.parse

from django.core.cache import cache
from django.http import JsonResponse, HttpResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from slack_sdk import WebClient
from slack_sdk.signature import SignatureVerifier

from app_bot import cmds
from app_bot.cmds import ExerciseListCommand
from core_config.settings import SLACK_SIGNING_SECRET, SLACK_TOKEN, CHANNEL_ID


class ViewBotMixin(View):
    slack_signing_secret = SLACK_SIGNING_SECRET
    slack_token = SLACK_TOKEN
    channel_id = CHANNEL_ID

    signature_verifier = SignatureVerifier(slack_signing_secret)
    client = WebClient(token=slack_token)

    commands = [
        cmds.InfoCommand(),
        cmds.RegistrationUserCommand(),
        cmds.ExerciseListCommand(),
        cmds.TrainingStartCommand(),
    ]

    def process_message(self, message):
        if message.get("channel") == self.channel_id and 'subtype' not in message:
            for command in self.commands:
                if message.get("text") in command.command:
                    command.handle(client=self.client, message=message)


@method_decorator(csrf_exempt, name='dispatch')
class SlackEventView(ViewBotMixin):
    def post(self, request):
        body = request.body
        if not self.signature_verifier.is_valid_request(body, request.headers):
            return JsonResponse({"status": "Invalid request"})

        event_data = json.loads(body)
        event_type = event_data["type"]

        if event_type == "url_verification":
            return JsonResponse({"challenge": event_data["challenge"]})

        if event_type == "event_callback":
            message = event_data["event"]
            if message.get("type") == "message":
                self.process_message(message)

        return HttpResponse(status=200)


@method_decorator(csrf_exempt, name='dispatch')
class SlackActionView(ViewBotMixin):

    def post(self, request):
        body = request.body.decode()
        payload = json.loads(urllib.parse.parse_qs(body)['payload'][0])
        ptype = payload['type']
        self.user = payload['user']['id']

        handler = getattr(self, f'handle_{ptype}', None)

        if handler:
            # calls handle_block_actions or handle_view_submission method
            return handler(payload)

        return HttpResponse(status=400)

    def handle_block_actions(self, payload):
        action_id = payload['actions'][0]['action_id']
        handler = getattr(self, f'handle_block_{action_id}', None)

        if handler:
            # calls any one handle_block_*** method
            return handler(payload)

        return HttpResponse(status=400)

    def handle_view_submission(self, payload):
        callback_id = payload['view']['callback_id']
        handler = getattr(self, f'handle_view_{callback_id}', None)

        if handler:
            # calls any one handle_view_*** method
            return handler(payload)

        return HttpResponse(status=400)

    # handle_block
    def handle_block_open_registration_form(self, payload):
        if cmds.RegistrationUserCommand.check_registered(self.user):
            if cache.get(self.user):
                return HttpResponse(status=200)
            cmds.RegistrationUserCommand.send_user_already_registered(self.channel_id, self.client, self.user)
            cache.set(self.user, 'STOP SPAM', timeout=3600)
            return HttpResponse(status=200)

        self.open_form(payload['trigger_id'], cmds.RegistrationUserCommand.get_slack_user_form())
        return HttpResponse(status=200)

    def handle_block_open_exercise_form(self, payload):
        if not cmds.RegistrationUserCommand.check_registered(self.user):
            return self.client.chat_postMessage(channel=self.channel_id,
                                                text=f"<@{self.user}> Nejprve se musíte zaregistrovat, "
                                                     f"použijte registrační příkaz '-r'")
        self.open_form(payload['trigger_id'],
                       cmds.ExerciseListCommand().get_exercise_form(int(payload['actions'][0]['value']),
                                                                    self.user,
                                                                    payload['container']['message_ts'],
                                                                    callback_id='exercise_form')
                       )
        return HttpResponse(status=200)

    def handle_block_open_training_start_form(self, payload):
        self.open_form(payload['trigger_id'], cmds.TrainingStartCommand().get_training_start_form())
        return HttpResponse(status=200)

    def handle_block_open_exercise_training_form(self, payload):
        value_dict = json.loads(payload['actions'][0]['value'])
        exercise_id = value_dict.pop('exercise_id')
        self.open_form(payload['trigger_id'],
                       cmds.ExerciseListCommand().get_exercise_form(exercise_id=int(exercise_id),
                                                                    user_id=self.user,
                                                                    message_ts=payload['container']['message_ts'],
                                                                    callback_id='exercise_training_form',
                                                                    **value_dict)
                       )
        return HttpResponse(status=200)

    # handle_view
    def handle_view_registration_form(self, payload):
        cmds.RegistrationUserCommand.create_slack_user(payload)
        return HttpResponse(status=200)

    def handle_view_exercise_form(self, payload):
        cmds.ExerciseListCommand().update_or_create_training(payload)
        self.client.chat_update(
            channel=CHANNEL_ID,
            ts=json.loads(payload['view']['private_metadata'])['message_ts'],
            text='',  # Required as a fallback option in case the 'blocks' content cannot be displayed.
            blocks=ExerciseListCommand().blocks
        )
        return HttpResponse(status=200)

    def handle_view_exercise_training_form(self, payload):
        muscle_list = json.loads(payload['view']['private_metadata'])['body_parts'][0]
        cmds.ExerciseListCommand().update_or_create_training(payload)
        self.client.chat_update(
            channel=CHANNEL_ID,
            ts=json.loads(payload['view']['private_metadata'])['message_ts'],
            text='',    # Required as a fallback option in case the 'blocks' content cannot be displayed.
            blocks=cmds.TrainingStartCommand().update_or_create_progress_training(
                large_muscle=muscle_list['large_muscle'],
                small_muscle=muscle_list['small_muscle'],
                date=muscle_list['date'],
                user_id=self.user),
        )
        return HttpResponse(status=200)

    def handle_view_training_start_form(self, payload):
        values = payload['view']['state']['values']
        large_muscle = values['large_muscle_block']['large_muscle']['selected_option']['value']
        small_muscle = values['small_muscle_block']['small_muscle']['selected_option']['value']
        blocks = cmds.TrainingStartCommand().update_or_create_progress_training(large_muscle=large_muscle,
                                                                                small_muscle=small_muscle,
                                                                                user_id=self.user)
        if not blocks:
            self.client.chat_postMessage(channel=self.channel_id,
                                         text=f"<@{self.user}> Nejprve si musíte zaregistrovat cvičení!")
            return HttpResponse(status=200)
        self.client.chat_postMessage(
            channel=self.channel_id,
            blocks=blocks
        )
        return HttpResponse(status=200)

    def open_form(self, trigger_id, form):
        self.client.views_open(trigger_id=trigger_id, view=form)
