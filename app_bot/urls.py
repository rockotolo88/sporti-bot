from django.urls import path

from app_bot.views import SlackEventView, SlackActionView

urlpatterns = [
    path('event/', SlackEventView.as_view(), name='slack_events'),
    path('action/', SlackActionView.as_view(), name='slack_actions'),
]
